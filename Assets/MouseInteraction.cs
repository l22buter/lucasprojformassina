using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MouseInteraction : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
{

    private Rigidbody body;
    private Renderer rend;
    private Color pastColor;
    public int force = 1000;
    // Start is called before the first frame update
    void Start()
    {
        body=GetComponent<Rigidbody>();
        rend=GetComponent<Renderer>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnPointerClick(PointerEventData eventData)
    {
    
        body.AddForce(Camera.main.transform.forward*force, ForceMode.Impulse);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        rend.material.color=pastColor;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {   
        pastColor=rend.material.color;
        rend.material.color=Color.red;
    }
}
