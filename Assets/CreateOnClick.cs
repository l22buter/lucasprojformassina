using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class CreateOnClick : MonoBehaviour
{
    public InputAction throwCubes;
    public GameObject aCopier;
    public float copyDistanceFromCamera = 5.0f;
    private int force = 10000;

    // Start is called before the first frame update
    void Start()
    {
        throwCubes.Enable();
        throwCubes.started += throwFunction;
    }

    private void throwFunction(InputAction.CallbackContext context)
    {
        Debug.Log("well entered in the throw function");
        GameObject newObj = Instantiate(aCopier);
        newObj.GetComponent<Renderer>().material.color=Color.yellow;
        newObj.transform.position=Camera.main.transform.position+Camera.main.transform.forward * copyDistanceFromCamera;
        newObj.GetComponent<Rigidbody>().AddForce(Camera.main.transform.forward*force, ForceMode.Impulse);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
